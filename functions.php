<?php


/**
 * function leggi($fileName)
 * return the matrix if all it's okay 
 * return false if anything it's not okay 
 */

  function leggi ($fileName){

    // check open file 
    if ( @! $fp = fopen($fileName,"r") ):
        echo "file non trovato"; // print it
        return false; // return false
    endif;

    // read file
    while(!feof($fp))
      $data[] = fgetcsv($fp); // create matrix
      
    fclose($fp); //close file

    $lastElement = count($data) - 1; // get last element in array

    if(empty($data[$lastElement])) // check if not empty
        unset($data[$lastElement]); // if true i delete it

      return ! empty($data) ? $data : false ; // check if data not empty
  }




/**
 * function estrai($taglia, $matrice)
 * return the matrix if all it's okay 
 * return false if anything it's not okay 
 */

function estrai($taglia, $matrice){
  if( empty($matrice))
    return false ; // check if matrice empty

  for ($i=0; $i < count($matrice) ; $i++) 
      if($matrice[$i][1] == $taglia ) 
        $matrix[] = $matrice[$i];
  

  return ! empty($matrix) ? $matrix : false ; // check if matrix not empty
}

/**
 * function totQuant($matrice)
 * return the total number of items
 */
function totQuant($matrice){
  $tot = 0;
  $dim_i = count($matrice);
  $i = 0;
  $quantity = 2; //posizione nella matrice valore quantità

  for($i; $i < $dim_i; $i++){
    $tot += $matrice[$i][$quantity];
  }

  return $tot;
}

/**
 * function totPrice($matrice)
 * return the total price of all the items
 */
 function totPrezzo($matrice){
  $tot = 0;
  $dim_i = count($matrice);
  $i = 0;
  $quantity = 2; //posizione nella matrice valore quantità
  $price = 3; //posizione nella matrice prezzo

  for($i; $i < $dim_i; $i++){
    $tot += ( $matrice[$i][$quantity] * $matrice[$i][$price] );
  }

  return $tot;
}

/**
 * tot()
 *
 * @param array $vestiti
 * @return array
 */
function tot($vestiti) {
     /** @var array $totali che contera i totali per taglia */
     $totali = array();
     // processare i vestit
     for ($taglia = 42; $taglia <= 54; $taglia += 2) {

         $vestitiPerTaglia = estrai($taglia, $vestiti); // estrazione vestiti per taglia

         $quantita = totQuant($vestitiPerTaglia); // calcolo quantita per taglia

         $prezzoTotale = totPrezzo($vestitiPerTaglia); // calcolo prezzo totale per taglia

         $totali[] = array($taglia, $quantita, $prezzoTotale);
     }
     return $totali;
}

function visTabella($vestiti){
    echo "<h1 style='text-align: center'>Show Totali</h1><table>
    <tr><th>Taglia</th><th>Quantita</th><th>Prezzo</th></tr>";

    for ($i=0; $i < count($vestiti); $i++) { 
        echo "<tr>";
        for($j=0; $j < count($vestiti[$i]); $j++) { 
            echo "<td>".$vestiti[$i][$j]."</td>";
        }
        echo "</tr>";
    }
    echo "<table>";
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body{
            background: rgb(69,72,77);
            background: -moz-linear-gradient(left,  rgba(69,72,77,1) 0%, rgba(0,0,0,1) 100%); /* FF3.6-15 */
            background: -webkit-linear-gradient(left,  rgba(69,72,77,1) 0%,rgba(0,0,0,1) 100%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to right,  rgba(69,72,77,1) 0%,rgba(0,0,0,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#45484d', endColorstr='#000000',GradientType=1 ); /* IE6-9 */
            }
        table{
            margin: auto;
            width: 50%;
            text-align: center;
        }
        th{
            color: rgb(240, 242, 243);
            border-top: solid rgb(238, 238, 238) 2px;
            border-bottom: solid rgb(136, 136, 136) 2px;
            width: calc(40% / 3);
            border-radius: 10px;
            height: 30px;
        }
        td{
            color: rgb(240, 242, 243);
            border-bottom: solid rgb(139, 139, 139) 2px;
            height: 30px;
            border-radius: 8px;
        }
        h1{
            color: rgb(240, 242, 243);
            text-align: center;
            font-family: cursive;
        }
        h4{
            margin: 0;
            text-align: center;
            background-color: antiquewhite;
        }
        ul {
            margin: auto;
            margin-top: 5%;
            list-style-type: none;
            width: 30%;
            padding: 0;
        }
        li{
            box-shadow: rgb(72, 70, 70) 0px 3px 4px 2px;  
            padding: 2%;
            border-radius: 5px;
            margin-top: 5%;
            transition: 2s;
        }
        li:hover{
            transform: scale(1.05);
            box-shadow: rgb(97, 97, 97) 0px 2px 4px 2px;  
            padding: 1%;
            padding: 2%;
        }
        a{
            color: rgb(240, 242, 243);
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            text-decoration: none;   
        }
        .codice{
            color: rgb(0, 170, 255);
        }
        .menu{
            text-align: center;
            padding: 1%;
        }
    </style>
    <title>Document</title>
</head>
<body>
    <h4>per vedere il codice <a class="codice" target="_blank" href="https://gitlab.com/aligabbos/vestiti_pensati_2.0">cliccare qua!</a></h4>
    <nav class="menu">
        <ul>
            <a href="index.php?req="><li>Reset</li></a>
            <a href="index.php?req=visualizza"><li>Visualizza</li></a>
        </ul>
    </nav>

    <!-- Le chiamate delle funzioni da function.php -->
    <?php
        //Il controllo sulla richessta che viene effettuata
        $request =  (isset($_REQUEST['req'])) ? $_REQUEST['req'] : "";        

        switch ($request){
            //quando la richiesta è visualizza chiama le funzioni che ci serve
            case "visualizza":
                //le funzioni
            default:
                include_once("index.php");
                break;
        }
    ?>
    <!-- --------------------------------------------- -->

</body>
</html>